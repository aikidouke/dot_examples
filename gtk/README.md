## GTK

For GTK I make use of oomox (numix fork with a color changing script), so that I may conform my colors to my current colorscheme. Typically my themes go for one of two style types:

subtle | invert
-------|--------
![subtle](https://u.teknik.io/3dclwG.png) | ![invert](https://u.teknik.io/bYAvQA.png)

The cool thing here is that the icons don't clash - that's done with ACYL, an icon set that comes with the ability to change all the colors of the icons. I have a fork which adds a shell script to change them all to a specified color. In combination, oomox + acyl make a really flexible gtk setup.
