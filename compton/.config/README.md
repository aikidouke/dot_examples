## Compton

This compton config is based on dkegs sourced circa june 2015. 

### Notes:
- Transparent window borders
- Shadows on panel/dock window types
- Adds a fade time for new windows/workspace transitions
- includes a shadow-exclude list to name windows to not draw shadows on
	- I use this for making certain panels in themes have no shadows
